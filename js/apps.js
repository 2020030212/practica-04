arreglo = [15, 21, 83, 74, 75, 46, 27, 68, 99, 18, 55, 65, 47, 10, 47, 91, 100, 12, 56, 50];

function prom(arreglo) {
    promedio = 0.0;
    suma = 0;
    for (i = 0; i < 20; i++) {
        suma = suma + arreglo[i];
    }
    promedio = suma / 20;
    result = document.getElementById('resultado')
    result.innerHTML = "El promedio es: " + promedio;
}

function par(arreglo) {
    sumas = 0;
    for (i = 0; i < 20; i++) {
        if (arreglo[i] % 2 == 0) {
            sumas++;
        }
    }
    resultado = document.getElementById('resultado')
    resultado.innerHTML = "El número de pares es: " + sumas;
}

function mayoramenor(arreglo) {
    arreglo.sort((a, b) => {
        if (a == b) {
            return 0;
        } else if (a > b) {
            return -1;
        } else {
            return 1;
        }
    })
    resultado = document.getElementById('resultado')
    resultado.innerHTML = "Los números de mayor a menor son: " + arreglo;
}

// 20/10/2022

var num = [];

function llenar() {
    var limite = document.getElementById('limite').value;
    var listanumeros = document.getElementById('numeros');
    listanumeros.innerHTML = "";
    num.length = 0;
    var aleatorio = 0;

    while (listanumeros.length > 0) {
        listanumeros.remove(0);
    }

    for (let i = 0; i < limite; i++) {
        res = 0;
        aleatorio = Math.random();
        aleatorio = aleatorio * 50 + 1;
        aleatorio = Math.trunc(aleatorio);
        res = aleatorio;
        num.push(res);
    }
    num.sort((a, b) => {
        if (a == b) {
            return 0;
        }
        if (a < b) {
            return -1;
        }
        return 1;
    })
    for (let i = 0; i < limite; i++) {
        listanumeros.options[i] = new Option(num[i], 'valor', +i);
    }
    pares(num);
}

function pares(num) {
    var limite = document.getElementById('limite').value;
    pa = 0;
    impar = 0;
    for (i = 0; i < limite; i++) {
        if (num[i] % 2 == 0) {
            pa++;
        }
        else {
            impar++;
        }
    }
    var porpar = 0.0;
    porpar = pa / limite * 100;
    var porimpar = 0.0;
    porimpar = impar / limite * 100;

    document.querySelector("#porpares").innerHTML = porpar.toFixed(0) + "%";
    document.querySelector("#porimpares").innerHTML = porimpar.toFixed(0) + "%";

    porpar = porpar.toFixed(0);
    porimpar = porimpar.toFixed(0);
    var dif = 0.0

    if(porpar < porimpar){
        dif = porimpar - porpar;
    }if (porpar > porimpar) {
        dif = porpar - porimpar;
    } 

    if (dif <= 25) {
        document.querySelector("#EsSimetrico").innerHTML = "Si es Símetrica"
    } if(dif > 25) {
        document.querySelector("#EsSimetrico").innerHTML = "No es Símetrica"
    } if (porpar == porimpar) {
        document.querySelector("#EsSimetrico").innerHTML = "No es Símetrica"
    }
}

//Commits
//Generacion de numeros aleatorios
//
//
//contar los numeros pares e impares para saber su porcentaje
//La diferencia no sea mayor al 25%